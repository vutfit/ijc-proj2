# IJC, proj2 makefile
# Jan Svabik (xsvabi00)
# Faculty of Information Technology, BUT
# 2019-04-20

.PHONY = all pack cleanlibs

# COMPILERS
GCC = gcc -std=c99 -Wall -pedantic -Wextra -g
GPP = g++ -std=c++11 -Wall -pedantic -Wextra

# LIBRARY LIST
LIB_LIST = htab_add.o htab_begin.o htab_bucket_count.o htab_clear.o htab_end.o htab_free.o htab_hash_function.o htab_init.o htab_iterator_get_key.o htab_iterator_get_value.o htab_iterator_next.o htab_iterator_set_value.o htab_lookup_add.o htab_move.o htab_size.o io.o

all: wordcount wordcount-dynamic tail tail2 cleanlibs

pack:
	zip xsvabi00.zip *.c *.cc *.h Makefile

cleanlibs:
	rm -f *.o

# PROGRAMS
wordcount: wordcount.o libhtab.a
	$(GCC) wordcount.o libhtab.a -static -o wordcount

wordcount-dynamic: wordcount.o libhtab.so
	$(GCC) -dynamic wordcount.o libhtab.so -o wordcount-dynamic

tail: tail.c
	$(GCC) tail.c -o tail

tail2: tail2.cc
	$(GPP) tail2.cc -o tail2

# LIBRARIES
wordcount.o: wordcount.c
	# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./
	$(GCC) -c wordcount.c -o wordcount.o

libhtab.so: $(LIB_LIST)
	$(GCC) -shared -fPIC $(LIB_LIST) -o libhtab.so

libhtab.a: $(LIB_LIST)
	ar rcs libhtab.a $(LIB_LIST)

# MODULES
$(LIB_LIST): %.o: %.c
	$(GCC) -c -fPIC $< -o $@
