/**
 * @file htab_iterator_get_key.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_iterator_get_key
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Get key from iterator
 * Gets key from the iterator specified.
 * @param it the iterator to get key from
 * @return the key
 */
const char* htab_iterator_get_key(htab_iterator_t it) {
    return it.ptr->key;
}
