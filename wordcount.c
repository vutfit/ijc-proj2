/**
 * @file wordcount.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, wordcount
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief The number of rows in the first table.
 * Why 16000: I tested 7 books from the first page from gutenberg.com (sorted by popularity). They contained some number of words and I calculated the average of them (in thousands): (13+6+19+13+27+6+27)/7=15.86. However, the data sample is probably too low (but at least I told you my idea).
 */
#define START_TABLE_BUCKET 16000

/**
 * @brief The max length of word.
 */
#define MAX_WORD_LENGTH 127

bool htab_iterator_equal();
bool htab_iterator_valid();

int main(void) {
    // init the table
    unsigned maxWordsStart = START_TABLE_BUCKET;
    htab_t* table = htab_init(maxWordsStart);

    // load each word from stdin
    char word[MAX_WORD_LENGTH+1];
    while (get_word(word, MAX_WORD_LENGTH, stdin) != -1) {
        // get word iterator and increment word counter
        htab_iterator_t iterator = htab_lookup_add(table, word);
        iterator.ptr->data++;

        // if the table is full, double the bucket
        if (htab_size(table) == htab_bucket_count(table))
            table = htab_move((maxWordsStart *= 2), table);
    }

    // print the word count
    htab_iterator_t i = htab_begin(table);
    htab_iterator_t last = htab_end(table);

    // print the data
    while (!htab_iterator_equal(i, last)) {
        printf("%s\t%d\n", htab_iterator_get_key(i), htab_iterator_get_value(i));
        i = htab_iterator_next(i);
    }
    
    // deallocate the table
    htab_clear(table);
    return 0;
}
