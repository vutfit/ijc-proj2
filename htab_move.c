/**
 * @file htab_move.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_move
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Create new table and move data from old table to the new.
 * Allocates memory for new table, copy all data from the @p from table and store it in the new table. Pointer to the new table will be returned and the old table will be freed.
 * @param n new table size
 * @param from pointer to the old table (where the data move from)
 * @return Pointer to the new table
 */
htab_t *htab_move(size_t n, htab_t *from){
    // allocate new table
    htab_t *table = htab_init(n);

    // control iterators
    htab_iterator_t i = htab_begin(from);
    htab_iterator_t last = htab_end(from);

    // for storing current key in each iteration
    htab_iterator_t currentKey;

    // copy the data
    while (!htab_iterator_equal(i, last)) {
        currentKey = htab_lookup_add(table, htab_iterator_get_key(i));
        htab_iterator_set_value(currentKey, htab_iterator_get_value(i));

        i = htab_iterator_next(i);
    }

    // deallocate the old table and return the pointer to the new table
    htab_clear(from);
    return table;
}
