/**
 * @file tail.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 1, a)
 * @version 1.0
 * @date 2019-03-28
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define LINE_ALLOC_LENGTH 1024

/**
 * @brief Check the program arguments (number of, value)
 * Checks the program arguments and if there is a problem, prints and error
 * @param argc The number of program arguments
 * @param argv The program arguments
 * @return true if there is no error
 * @return false if there is an error with the arguments
 */
bool checkArguments(int argc, char** argv) {
    // wrong syntax
    if ( (argc > 4) || (argc > 2 && strcmp(argv[1], "-n") != 0) || (argc == 2 && strcmp(argv[1], "-n") == 0) ) {
        fprintf(stderr, "Wrong syntax. The right syntax is:\n./tail [-n number] file.txt\n./tail [-n number] [< file.txt]\n");
        return false;
    }

    return true;
}

/**
 * @brief Determine how many lines to store
 * Extract the number of lines from the arguments and store in @p loadLines
 * @param lines The program argument
 * @param loadLines Pointer to the variable to store the number of lines user wants to read
 * @param jumping how many lines we need to jump over
 * @return true if there is no error
 * @return false if there is an error with the argument(s)
 */
bool setLoadLines(char* lines, unsigned long* loadLines, unsigned long* jumping) {
    // extract the number from the string
    char* pattern;
    *loadLines = strtol(lines, &pattern, 10);
    if (strcmp(pattern, "\0") != 0) {
        fprintf(stderr, "The value of -n parameter have to be a number.\n");
        return false;
    }

    // if there was a plus symbol, save the jumping flag
    if (lines[0] == '+')
        *jumping = *loadLines;

    // minus has not to be the first character of the -n value
    if (lines[0] == '-' || *loadLines == 0) {
        fprintf(stderr, "The number of lines have to be greater than zero.\n");
        return false;
    }

    return true;
}

/**
 * @brief Memory allocation
 * Allocates memory for the array of lines and the lines itself
 * @param numberOfLines How many lines we will want to store
 * @return pointer to the start of the allocated memory
 */
char** allocateLineArray(unsigned long numberOfLines) {
    // allocate memory for the whole array
    char** lines = malloc(sizeof(char*) * numberOfLines);
    if (lines == NULL) {
        fprintf(stderr, "There was a problem with memory allocation. Sorry.\n");
        return NULL;
    }

    // allocate memory for each line
    for (unsigned long i = 0; i < numberOfLines; i++)
        lines[i] = malloc(sizeof(char) * LINE_ALLOC_LENGTH);

    return lines;
}

/**
 * @brief Memory deallocation
 * Deallocates the memory allocated for the lines and for the array of lines.
 * @param lines Pointer to the array with stored lines
 * @param numberOfLines Number of stored lines
 */
void deallocateLineArray(char** lines, unsigned long numberOfLines) {
    for (unsigned long i = 0; i < numberOfLines; i++)
        free(lines[i]);
    free(lines);
}

/**
 * @brief Put new line into the buffer
 *
 * @param line Content of the line to store
 * @param lines Pointer to the array of lines
 * @param numberOfLines How many lines to store
 * @return unsigned long index where to store the next line
 */
unsigned long putInfoBuffer(char* line, char** lines, unsigned long numberOfLines, unsigned long jumping) {
    static unsigned long i = 0;

    // start from the 0th index if the array is full
    if ((!jumping && i == numberOfLines) || (jumping && i >= numberOfLines-jumping))
        i = 0;

    // add the line to the array of lines and remove newline chars
    printf("Na pozici %lu ukladam %s", i, line);
    strcpy(lines[i], line);
    lines[i][strcspn(lines[i], "\r\n")] = 0;

    return ++i;
}

/**
 * @brief Load and store the lines
 * Load lines from the file into the buffer. If there is some too long line, print an error message and strip the line.
 * @param file Pointer to the file to store lines from
 * @param lines Pointer to the allocated array for the line storing
 * @param numberOfLines How many lines to store
 * @param jumping how many lines we need to jump over
 * @return unsigned long line where to start reading for print
 */
unsigned long storeLines(FILE* file, char** lines, unsigned long numberOfLines, unsigned long jumping) {
    // if the line limit was written, do not write it again
    bool lineLimitExceededErrorWritten = false;

    // the current line is new line from the file
    bool toBuffer = true;

    // line buffer and line counter
    char line[LINE_ALLOC_LENGTH];
    unsigned long loadedLines = 0;

    // the loading loop
    unsigned long start = 0;
    while (fgets(line, LINE_ALLOC_LENGTH, file) != NULL) {
        // copy the line into the buffer and remove the (\r)\n symbol
        printf("Testing %s", line);
        if (toBuffer && (!jumping || jumping <= loadedLines)) {
            start = putInfoBuffer(line, lines, numberOfLines, jumping);
        }

        // determine that the line is new or too long
        if (strstr(line, "\n") != NULL) {
            // the loaded line long is ok (contains \n symbol)
            toBuffer = true;
            loadedLines++;
        }
        else {
            // this part of line does not include \n -> it is too long
            toBuffer = false;
            if (!lineLimitExceededErrorWritten && loadedLines < numberOfLines) {
                fprintf(stderr, "Some line(s) too long. Printing only first 1023 characters from these lines.\n");
                lineLimitExceededErrorWritten = true;
            }
        }
    }

    return start;
}

/**
 * @brief Prints the stored lines.
 * Prints the lines stored in @p lines array.
 * @param lines Pointer to the array of lines
 * @param numberOfLines The length of the @p lines array
 * @param readStart Index where to start reading lines from the @p lines array
 */
void printLines(char** lines, unsigned long numberOfLines, unsigned long readStart, unsigned long jumping) {
    if (readStart >= numberOfLines-jumping)
        readStart = 0;

    for (unsigned long i = 0; i < numberOfLines-jumping; i++) {
        printf("%s\n", lines[readStart++]);
        if (readStart > numberOfLines-jumping-1)
            readStart = 0;
    }
}

/**
 * @brief Count the file lines
 * Get the number of the file lines and change loadLines if necesarry.
 * @param file Pointer to the file to count lines of
 * @param loadLines Pointer to the number to change if necessary.
 * @param jumping how many lines we need to jump over
 * @return true if there was no error
 * @return false if there was error, e.g. with reseting the file pointe
 */
bool checkFileLines(FILE* file, unsigned long* loadLines, unsigned long jumping) {
    // count the file lines
    unsigned long lines = 0;
    int c;
    while ((c = fgetc(file)) != EOF) {
        if (c == '\n')
            lines++;
    }

    // change the loadLines if necessary
    if (lines < *loadLines || jumping)
        *loadLines = lines;

    // subtract the value we jump to
    if (jumping)
        lines -= *loadLines;

    // reset file pointer
    if (fseek(file, 0L, SEEK_SET) != 0) {
        fprintf(stderr, "Cannot reset the file pointer.\n");
        return false;
    }

    return true;
}

/**
 * @brief The program entry point.
 * The basic functionality - file opening, print call, deallocating call etc.
 * @param argc Number of program arguments
 * @param argv Array with program arguments
 * @return int Program status
 */
int main(int argc, char** argv) {
    // declare number of lines variable
    unsigned long loadLines = 10;
    unsigned long jumping = 0;

    // check the arguments, if it fails, end the program
    if (!checkArguments(argc, argv))
        return EXIT_FAILURE;

    // load number of files
    if (argc >= 2 && strcmp(argv[1], "-n") == 0 && !setLoadLines(argv[2], &loadLines, &jumping))
        return EXIT_FAILURE;

    // get the filename
    char* filename = NULL;
    if (argc == 2)
        filename = argv[1];
    else if (argc == 4)
        filename = argv[3];

    // open the file (for read only)
    FILE* file;
    if (filename == NULL)
        file = stdin;
    else
        file = fopen(filename, "r");

    // check that the file was loaded succesfully
    if (file == NULL) {
        fprintf(stderr, "Cannot open the specified file. Maybe it does not exist?\n");
        return EXIT_FAILURE;
    }

    // get the number of the file lines and change loadLines if necessary
    if (!checkFileLines(file, &loadLines, jumping))
        return EXIT_FAILURE;

    // check that the user do not want jump more lines than really exists in file
    if (jumping > loadLines) {
        fprintf(stderr, "You cannot jump more lines than the file really has.\n");
        return EXIT_FAILURE;
    }

    // allocate memory for n lines
    char** lines = allocateLineArray(loadLines-(jumping?jumping+1:0));
    if (lines == NULL)
        return EXIT_FAILURE;

    // store the lines and get the position of the first line to print
    unsigned long readStart = storeLines(file, lines, loadLines, jumping);

    // print the stored lines
    printLines(lines, loadLines, readStart, jumping);

    // deallocation
    deallocateLineArray(lines, loadLines-jumping);
    fclose(file);

    return EXIT_SUCCESS;
}
