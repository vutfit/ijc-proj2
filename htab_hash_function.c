/**
 * @file htab_hash_function.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_hash_function
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Calculate the hash
 * Calculates hash for the string given
 * @param str string to hash
 * @return unsigned int the hash calculated
 */
unsigned int htab_hash_function(const char *str) {
    uint32_t h=0; // musí mít 32 bitů
    const unsigned char *p;
    for (p = (const unsigned char*)str; *p != '\0'; p++)
        h = 65599*h + *p;

    return h;
}
