/**
 * @file htab_iterator_next.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_iterator_next
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Get next iterator
 * Gets the next iterator (after the iterator specified in @p it argument).
 * @param it iterator to search the next iterator of
 * @return the next iterator (or htab_end)
 */
htab_iterator_t htab_iterator_next(htab_iterator_t it) {
    // if the pointer points to NULL, it could be the table end of list end
    if (it.ptr == NULL) {
        if (it.idx < it.t->arr_size-1) {
            // it is only list end if we are not at the last row of the table
            it.ptr = it.t->list[++it.idx];
        }
        else {
            // it must be the table end
            return htab_end(it.t);
        }
    }

    // next word in the list
    else {
        it.ptr = it.ptr->next;
    }

    // return next until the valid one
    if (!htab_iterator_valid(it))
        return htab_iterator_next(it);

    return it;
}
