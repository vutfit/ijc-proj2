/**
 * @file htab_free.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_free
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Free the table
 * Deallocate the memory for the table @p t.
 * @param t pointer to the table to deallocate
 */
void htab_free(htab_t* t){
    free(t);
}
