/**
 * @file io.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, io
 * @version 1.0
 * @date 2019-03-28
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

/**
 * @brief Get the next word from file
 * Loads the next word from the file specified.
 * @param s where to store the word
 * @param max maximum word length
 * @param f file where to load from
 * @return int the length of the loaded word or -1 if EOF reached
 */
int get_word(char *s, int max, FILE *f) {
    static bool lineLengthLimitExceededErrorWritten = false;
    int c, length = 0;

    // reset the word storage
    memset(s, 0, max+1);

    while ( (c = fgetc(f)) != EOF ) {
        length = strlen(s);

        // standard character and max not reached yet
        if (!isspace(c) && length < max)
            s[length] = c;
        
        // maximum exceeded
        else if (!isspace(c) && length == max) {
            s[length] = 0;

            // error printing
            if (!lineLengthLimitExceededErrorWritten) {
                printf("Max line limit length exceeded. Stripping it (and the next too long lines too).\n");
                lineLengthLimitExceededErrorWritten = true;
            }
        }

        // standard whitespace without limit exceeding
        else if ((isspace(c) || c == EOF) && length > 0) {
            s[length] = 0;
            break;
        }

        // nothing to do
        else
            continue;
    }

    // end of file reached, the loaded string must have 0 length
    if (c == EOF)
        return -1;

    return strlen(s);
}
