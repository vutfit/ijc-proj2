/**
 * @file htab_size.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_size
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Get table size
 * Returns the number of items in table specified.
 * @param table to get size of
 * @return number of items in table
 */
size_t htab_size(const htab_t * t) {
    return t->size;
}
