/**
 * @file htab_iterator_get_value.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_iterator_get_value
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Get value by iterator
 * Get value of the record specified by the iterator @p it.
 * @param it iterator to get value from
 * @return int the value of the record
 */
int htab_iterator_get_value(htab_iterator_t it) {
    return it.ptr->data;
}
