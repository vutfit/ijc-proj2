/**
 * @file tail.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 1, b)
 * @version 1.0
 * @date 2019-04-02
 */

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <string.h>
#include <vector>

using namespace std;

bool checkArguments(int argc, char** argv) {
    // wrong syntax
    if ( (argc > 4) || (argc > 2 && strcmp(argv[1], "-n") != 0) || (argc == 2 && strcmp(argv[1], "-n") == 0) ) {
        cerr << "Wrong syntax. The right syntax is:\n./tail [-n number] file.txt\n./tail [-n number] [< file.txt]\n";
        return false;
    }

    return true;
}

// get the number of lines to load from string
bool setLoadLines(char* lines, unsigned long* loadLines, unsigned long* jumping) {
    // extract the number from the string
    char* pattern;
    *loadLines = strtol(lines, &pattern, 10);
    if (strcmp(pattern, "\0") != 0) {
        cerr << "The value of -n parameter have to be a number.\n";
        return false;
    }

    // if there was a plus symbol, save the jumping flag
    if (lines[0] == '+') 
        *jumping = *loadLines;

    // minus has not to be the first character of the -n value
    if (lines[0] == '-' || *loadLines == 0) {
        cerr << "The number of lines have to be greater than zero.\n";
        return false;
    }

    return true;
}

// print the stored lines
void printLines(vector<string> lines) {
    for (unsigned long i = 0; i < lines.size(); i++)
        cout << lines[i] << "\n";
}

// load the specified number of lines from the file (or stdin)
vector<string> getTheLines(bool usestdin, fstream& tfile, unsigned long loadLines, unsigned long jumping) {
    vector<string> lines;
    unsigned long loaded = 0;
    string line;
    while (usestdin ? getline(cin, line) : getline(tfile, line)) {
        // save new line to 'line'
        istringstream iss(line);

        // add this line to the vector and increment the number of loaded lines
        if (!jumping || loaded >= jumping-1)
            lines.push_back(line);
        loaded++;

        // if there are more lines loaded than wanted, remove the first element of the vector
        if (!jumping && loaded > loadLines)
            lines.erase(lines.begin());
    }

    return lines;
}

int main(int argc, char* argv[]) {
    // declare variables for storing the data
    vector<string> lines;
    fstream tfile;
    unsigned long loadLines = 10;
    unsigned long jumping = 0;

    // check arguments and get the lines maximum
    if (!checkArguments(argc, argv) || (argc >= 2 && strcmp(argv[1], "-n") == 0 && !setLoadLines(argv[2], &loadLines, &jumping)))
        return EXIT_FAILURE;

    // change usestdin to false if file was specified
    char* filename = NULL;
    if (argc == 2)
        filename = argv[1];
    else if (argc == 4)
        filename = argv[3];
    
    // use stdin if no file specified
    bool usestdin = (filename == NULL);

    // if some file was specified, open it
    if (!usestdin) {
        tfile.open(filename);

        // cannot open the file
        if (tfile.fail()) {
            cerr << "Cannot open the specified file. Maybe it does not exist?\n";
            return EXIT_FAILURE;
        }
    }

    // stream the file and print the stored lines
    printLines(getTheLines(usestdin, tfile, loadLines, jumping));
    
    return EXIT_SUCCESS;
}