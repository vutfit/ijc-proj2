/**
 * @file htab_begin.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_begin
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Get the first record in the table
 * Gets the first record in the table @p t.
 * @param t the table where to get the first record from
 * @return iterator to the record
 */
htab_iterator_t htab_begin(const htab_t * t) {
    htab_iterator_t iterator;
    iterator.idx = 0;
    iterator.ptr = t->list[0];
    iterator.t = t;

    // not found, try the next pointer
    if (iterator.ptr == NULL)
        return htab_iterator_next(iterator);

    return iterator;
}
