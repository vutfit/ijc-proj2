/**
 * @file htab_iterator_set_value.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_iterator_set_value
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Set value by iterator
 * Set value of the record specified by the iterator @p it.
 * @param it iterator to set value to
 * @param val value to set
 * @return int the saved value
 */
int htab_iterator_set_value(htab_iterator_t it, int val) {
    return (it.ptr->data = val);
}
