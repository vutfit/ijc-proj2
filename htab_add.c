/**
 * @file htab_add.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_add
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Create new item
 * Create new item and return pointer to it. Includes allocation for the item and for the key (flexible array member).
 * @param key key to add
 * @return htab_item_t* Pointer to the whole item added
 */
htab_item_t* htab_add(const char* key) {
    htab_item_t* item = malloc(sizeof(htab_item_t));

    if (item == NULL) {
        fprintf(stderr, "Nepovedlo se alokovat htab_item_t* item v htab_add.\n");
        exit(1);
    }

    item->key = malloc(sizeof(char[strlen(key)+1]));
    strcpy(item->key, key);
    item->data = 0;
    item->next = NULL;

    return item;
}
