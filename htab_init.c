/**
 * @file htab_init.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_init
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Init table
 * Allocates memory for the table and for specified number of items (rows). Set all pointer to NULL.
 * @param n number of rows to allocate (table size)
 * @return pointer to the allocated and initialized table
 */
htab_t* htab_init(size_t n) {
    htab_t* tptr = malloc(sizeof(htab_t) + sizeof(htab_item_t*)*n);

    tptr->size = 0;
    tptr->arr_size = n;

    for (size_t i = 0; i < n; i++)
        tptr->list[i] = NULL;

    return tptr;
}
