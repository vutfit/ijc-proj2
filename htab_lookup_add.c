/**
 * @file htab_lookup_add.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_lookup_add
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Get item or add if not exist
 * Creates and returns an iterator to the @p key in table @p t. If the table does not contain the key, it will be added.
 * @param t table where to search for the key
 * @param key key to search
 * @return the key iterator in the table
 */
htab_iterator_t htab_lookup_add(htab_t *t, const char *key) {
    // get the index and the first item pointer
    unsigned int index = htab_hash_function(key) % t->arr_size;
    htab_item_t* ptr = t->list[index];
    htab_item_t* previous;

    // the item certainly does not exist
    if (ptr == NULL) {
        ptr = t->list[index] = htab_add(key);
        t->size++;
    }

    // the item might exist in the list
    else {
        while (ptr != NULL) {
            if (strcmp(ptr->key, key) == 0)
                break;

            previous = ptr;
            ptr = ptr->next;
        }

        // the list does not contain the key anyway, add it
        if (ptr == NULL) {
            ptr = previous->next = htab_add(key);
            t->size++;
        }
    }

    // create and return the iterator
    htab_iterator_t iterator;
    iterator.t = t;
    iterator.idx = index;
    iterator.ptr = ptr;

    return iterator;
}
