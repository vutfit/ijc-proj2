/**
 * @file htab_clear.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_clear
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Deallocate memory for the table members and then the whole table.
 * Deallocates memory used by table members (key, pointer) and then deallocates the whole table.
 * @param t table to deallocate
 */
void htab_clear(htab_t* t){
    htab_iterator_t i = htab_begin(t);
    htab_iterator_t last = htab_end(t);

    // deallocate for each table element which is not empty
    while (!htab_iterator_equal(i, last)) {
        free(i.ptr->key);
        free(i.ptr);

        i = htab_iterator_next(i);
    }

    htab_free(t);
}
