/**
 * @file htab_end.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_end
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Get the table end
 * Gets the last record in the table (or, more precisely, the first non-existing record in the table).
 * @param t table to get the end of
 * @return iterator to the end of the table
 */
htab_iterator_t htab_end(const htab_t* t) {
    htab_iterator_t it;
    it.t = t;
    it.idx = t->arr_size-1;
    it.ptr = t->list[it.idx];

    // go through the list until end
    while (it.ptr != NULL)
        it.ptr = it.ptr->next;

    return it;
}
