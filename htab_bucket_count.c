/**
 * @file htab_bucket_count.c
 * @author Jan Svabik (xsvabi00@stud.fit.vutbr.cz), Faculty of Information Technology, BUT
 * @brief IJC, proj2, part 2, htab_bucket_count
 * @version 1.0
 * @date 2019-03-28
 */

#ifndef __HTABLE_H__
#include "htab.h"
#endif

/**
 * @brief Get table bucket size
 * Returns the table size (how many 'rows' it has)
 * @param t table to get bucket size of
 * @return size_t table size (number of rows)
 */
size_t htab_bucket_count(const htab_t * t) {
    return t->arr_size;
}
